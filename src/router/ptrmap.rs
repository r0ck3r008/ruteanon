use crate::router::getlevel;
use crate::MAXDEPTH;

pub struct PtrNode<T> {
    hash: String,
    cli: T,
}

pub struct PtrMap<T> {
    hash: String,
    map: [Vec<PtrNode<T>>; MAXDEPTH],
}

impl<T> PtrMap<T> {
    pub fn new(hash: &str) -> Self {
        return Self {
            hash: String::from(hash),
            map: Default::default(),
        };
    }

    pub fn insert(&mut self, client: PtrNode<T>) -> bool {
        let lvl: usize = getlevel(&self.hash, &client.hash);
        if lvl == self.hash.len() {
            // Strings are the same
            return false;
        }
        if self.map[lvl].len() == MAXDEPTH {
            return false;
        } else {
            self.map[lvl].push(client);
            return true;
        }
    }

    pub fn find_peer(&self, s: &str) -> Option<&PtrNode<T>> {
        let lvl: usize = getlevel(&self.hash, s);
        if lvl == self.hash.len() {
            // Strings are the same
            return None;
        }
        if self.map[lvl].len() == 0 {
            return None;
        } else {
            return Some(&self.map[lvl][0]);
        }
    }
}
