pub mod objmap;
pub mod ptrmap;

pub fn getlevel(s1: &str, s2: &str) -> usize {
    let mut count: usize = 0;
    let v1: Vec<char> = s1.chars().collect();
    let v2: Vec<char> = s2.chars().collect();
    let mlen = if v1.len() < v2.len() {
        v1.len()
    } else {
        v2.len()
    };

    while count <= mlen {
        if &v1[count] != &v2[count] {
            break;
        }
        count += 1;
    }

    return count;
}
