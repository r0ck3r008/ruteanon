use crate::router::getlevel;
use std::collections::HashMap;

struct ObjNode {
    slice: String,
    hmap: HashMap<char, ObjNode>,
}

pub struct ObjMap {
    root: ObjNode,
    size: usize,
}

impl ObjNode {
    fn new(s: &str) -> Self {
        return Self {
            slice: String::from(s),
            hmap: HashMap::new(),
        };
    }

    fn hmap_update(&mut self, sin_opt: Option<&str>, lvl: usize, split: char) {
        let sin: &str = match sin_opt {
            None => &self.slice,
            Some(sin) => sin,
        };
        if let Some(node) = self.hmap.get_mut(&split) {
            node.insert(&sin[lvl..]);
        } else {
            self.hmap.insert(split, ObjNode::new(&sin[lvl..]));
        }
    }

    fn insert(&mut self, sin: &str) {
        let lvl = getlevel(&self.slice, sin);
        if let Some(split) = sin.chars().nth(lvl) {
            self.hmap_update(Some(sin), lvl, split);
        }

        if let Some(split) = self.slice.chars().nth(lvl) {
            self.hmap_update(None, lvl, split);
            let tmp: &str = &self.slice;
            self.slice = String::from(&tmp[0..lvl]);
        }
    }

    fn find(&self, sfin: &str) -> bool {
        let lvl = getlevel(&self.slice, sfin);
        if let Some(split) = sfin.chars().nth(lvl) {
            if let Some(node) = self.hmap.get(&split) {
                return node.find(&sfin[lvl..]);
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
}

impl ObjMap {
    pub fn new() -> Self {
        return Self {
            root: ObjNode::new(""),
            size: 0,
        };
    }

    pub fn insert(&mut self, s: &str) {
        self.root.insert(s);
    }

    pub fn exists(&self, s: &str) -> bool {
        return self.root.find(s);
    }
}
